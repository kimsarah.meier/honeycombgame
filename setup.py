from setuptools import setup, find_packages
import sys

if sys.version_info < (3, 5):
    sys.exit('Sorry, Python < 3.5 is not supported!')

setup(name='gym_honeycomb',
      version='0.0.1',
      author='Kim Sarah Meier, Henrike Meyer',
      author_email='kimsarah.meier@stud.uni-goettingen.de, henrike.meyer@stud.uni-goettingen.de',
      description='The HoneyCombGame as a custom gym-environment.',
      install_requires=['gym', 'numpy', 'pandas'],  # And any other dependencies needed
      packages=find_packages()
      )
