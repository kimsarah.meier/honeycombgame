from gym_honeycomb.envs.honeycomb_env import HoneyCombEnv


class HoneyCombMinorityEnv(HoneyCombEnv):
    """ The environment for the informed minority of the HoneyComb Game.
    In this game, two of the six reward fields return a higher reward!
    """

    # change reward function
    @property
    def reward_payoff_field(self):
        reward_list = []
        for agent in range(self.game.agents):
            payoff = 0
            reward = 0
            if self.game.agent_position[agent] in self.game.PAYOFF_FIELD:
                if self.game.agent_position[agent] in self.game.PAYOFF_SPECIAL:
                    payoff = 2
                else:
                    payoff = 1
            for pos in range(self.game.agents):
                if self.game.agent_position[agent] == self.game.agent_position[pos]:
                    reward += payoff

            reward_list.append(reward)
        return reward_list, sum(reward_list)