import numpy as np
import gym
from gym.spaces import Box
from gym_honeycomb.game.gamelogic import HoneyCombGame
import pprint
import copy
import math
import pandas as pd


class HoneyCombEnv(gym.Env):
    """ The HoneyComb Game \n
    Observation: Box(16,)
        Distance to other agent (9 values),
        distance to payoff fields (6 values), \n
        remaining moves
    Actions: Discrete(7)
        'UP', 'DOWN_RIGHT', 'DOWN_LEFT', 'DOWN', 'UP_LEFT', 'UP_RIGHT', 'PASS'
    Reward: The Number of Agents on the same reward field.
    Episode Termination:
        End of Experiment, all 15 moves used. \n
        Episode length is greater than max_turns.
    """

    # TODO max_turns
    # Wir müssen uns eine plausible Anzahl an maximalen Spielzügen überlegen:
    # Problem: PASS kann ja unendlich oft genutzt werden, oder zählen wir das zu MOVES?

    def __init__(self, max_turns=300):
        # Every environment comes with an action_space and an observation_space.
        # These attributes are of type Space
        # action space: UP, DOWN_RIGHT, DOWN_LEFT, DOWN, UP_LEFT, UP_RIGHT, PASS
        self.action_space = gym.spaces.Discrete(7)
        # observation space: what the agent can see in the environment
        self.observation_space = (Box(-np.inf, np.inf, shape=(17,), dtype=np.int8))
        self.max_turns = max_turns
        self.game = HoneyCombGame()
        self.done = None

        # call reset here to initialize gamefield
        self.reset()

    def step(self, action, verbose=False, verbose_state=False):
        """Implementation of the agent-environment interaction.
        Performs one time step of environment dynamics.
        :param action: an action provided by the agent
        :param verbose: bool to print pd.DataFrame with coordinates of agents from game
        :param verbose_state: bool to print current state info during game (default is False)
        :return: state (object), reward (float), done (boolean), info (dict)
        """
        # Check if action is in discrete action space
        assert self.action_space.contains(action), "%r (%s) invalid " % (action, type(action))

        # Information dict, mostly for debug
        info = {
            'Turn': self.game.turns_count,
            'Move_type': None,
            'Agent_ID': self.game.current_agent,
            'Remaining moves': None,
            'Reward': 0,
            'Old position': None,
            'New position': None
        }

        reward_sum = 0

        # Get position
        old_position = copy.copy(self.game.agent_position[self.game.current_agent])
        info['Old position'] = old_position

        # Action from agent: 0-6
        # action : 'PASS'
        if action == 6:
            self.game.storage[self.game.current_agent].append('pass')

            # Update info: Agent stays at current position
            info['Move_type'] = 'PASS'
            info['New position'] = old_position

        else:
            # Get coordinates from old position and change in coordinates with action (delta row/column)
            r0, c0 = old_position
            dr, dc = self.game.ACTIONS[action]
            new_position = (r0 + dr, c0 + dc)
            valid_move = self.game.check_action(new=new_position)

            # If valid_move=True update of game parameters
            if valid_move:
                self.game.agent_position[self.game.current_agent] = new_position
                self.game.update_gamefield()
                self.game.remaining_moves[self.game.current_agent] -= 1

                # Update info:
                info['Move_type'] = self.game.ACTIONS_NAME[action]
                info['New position'] = self.game.agent_position[self.game.current_agent]
            else:
                # Update info: Agent stays at current position
                info['Move_type'] = 'not moved'
                info['New position'] = old_position

            # Update game parameter
            self.game.turns_count += 1
            self.game.storage[self.game.current_agent].append(self.game.agent_position[self.game.current_agent])

        # Update info:
        info['Remaining moves'] = self.game.remaining_moves[self.game.current_agent]

        # Next agent and determine if environment is done
        self.game.current_agent, self.done = self.game.next_agent()

        # Environment is done when max_turns is reached or no agent is able to move
        if self.game.turns_count > self.max_turns or self.done:
            self.done = True
            rewards, reward_sum = self.reward_payoff_field
            info['Reward'] = reward_sum

            if verbose:
                print('Agent coordinates through the game: \n', self.storage_df)
                print('Payoff fields: ', self.game.PAYOFF_FIELD)
                print('Rewards:', rewards)

        # Print option for debug
        if verbose_state:
            pprint.PrettyPrinter(indent=4).pprint(info)
            print('\n Current state of gamefield: \n', self.game.gamefield)

        return np.array(self.get_observations, dtype=np.int8), reward_sum, self.done, info

    # At the end of an episode call reset_game
    def reset(self):
        """ At the end of an episode call reset_game to initialize a new gamefield.
        :return: numpy array of current state
        """
        # Initialize a new gamefield
        self.game.reset_game()

        # Return current state of environment
        return np.array(self.get_observations, dtype=np.int8)

    # reward function
    @property
    def reward_payoff_field(self):
        reward_list = []
        for agent in range(self.game.agents):
            n = 0
            if self.game.agent_position[agent] in self.game.PAYOFF_FIELD:
                for pos in range(self.game.agents):
                    if self.game.agent_position[agent] == self.game.agent_position[pos]:
                        n += 1
            reward_list.append(n)
        return reward_list, sum(reward_list)

    @property
    def get_observations(self):
        """Calculate current state of environment by calculating distance of current agent to other agents and reward
        fields. Observation includes current agent, distances and remaining moves.
        :return: List
        """
        if self.game.current_agent in range(10):
            dist = []
            a = copy.copy(self.game.agent_position)
            a.remove(self.game.agent_position[self.game.current_agent])
            b1, b2 = self.game.agent_position[self.game.current_agent]
            # Calculate distance from other agents
            for i in a:
                a1, a2 = i
                c = math.sqrt((a1 - b1) ** 2 + (a2 - b2) ** 2)
                dist.append(c)

            # Calculate distance from payoff field
            for j in self.game.PAYOFF_FIELD:
                a1, a2 = j
                c = math.sqrt((a1 - b1) ** 2 + (a2 - b2) ** 2)
                dist.append(c)

            # Create observation: current agent, distances and remaining moves
            obs = [self.game.current_agent]
            obs += dist
            obs.append(self.game.remaining_moves[self.game.current_agent])
        else:
            # If the environment is done: agent=None, return something as obs
            obs = [-1] * 17
            # print('No one is playing.')
        # Make sure observation has length of observation space
        assert len(obs) == 17
        return obs

    @property
    def positions(self):
        """ Returns current agent positions
        :return: dict
        """
        pos = dict()
        for a in range(self.game.agents):
            pos[a] = self.game.agent_position[a]
        return pos

    @property
    def storage_df(self):
        """ Stores the coordinates of the finished game for further usage.
        :return: pd.Dataframe
        """
        df = pd.DataFrame(self.game.storage)
        df.fillna(value='pass', inplace=True)
        if self.done:
            df['end_position'] = self.game.agent_position
        return df

    @property
    def agent(self):
        """ Returns current agent
        :return: A number
        """
        return self.game.current_agent

    def render(self):
        """ Prints gamefield as matrix.
        :return: numpy.ndarray
        """
        return self.game.gamefield

    def close(self):
        pass